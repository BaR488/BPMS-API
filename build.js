const fs = require('fs');

var refResolver = require('./utils/refResolver');

refResolver(function (results) {
    fs.writeFile('public/swagger.yaml', JSON.stringify(results, null, 2), { flag: 'w+' }, function (err) {
        if (err) {
            return console.log(err);
        }

        console.log('The swagger file was saved!');
    });
});