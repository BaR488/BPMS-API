module.exports = function (callback) {
    var resolve = require('json-refs').resolveRefsAt;
    var YAML = require('js-yaml');

    var file = 'swagger/api.yaml';

    var options = {
        filter        : ['relative', 'remote'],
        loaderOptions : {
            processContent : function (res, callback) {
                callback(null, YAML.safeLoad(res.text));
            }
        }
    };
    resolve(file, options).then(function (results) {
        callback(results.resolved);
    });
}