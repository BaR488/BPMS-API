var refResolver = require('./utils/refResolver');
const swaggerParser = require('swagger-parser');

refResolver(function (results) {
    swaggerParser.validate(results)
        .then(function (api) {
            console.log('Yay! The API is valid.');
        })
        .catch(function (err) {
            console.error('Onoes! The API is invalid. ' + err.message);
            process.exit(1);
        });
});